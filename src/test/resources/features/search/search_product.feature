Feature: Search for the product
  As a user
  I want to use API product search
  So that I can see positive and negative results

  Scenario Outline: Positive search w results
    When he calls search endpoint with productName "<productName>"
    Then he sees response status code <statusCode>
    Then he sees the results list displayed is "<isResultExpected>"
    Then he sees the results for "<productName>"
    Examples:
      | productName | statusCode | isResultExpected |
      | apple       | 200        | true             |
      | mango       | 200        | true             |
      | tofu        | 200        | true             |
      | water       | 200        | true             |

  Scenario Outline: Negative search w/o results
    When he calls search endpoint with productName "<productName>"
    Then he sees response status code <statusCode>
    Then he sees the results list displayed is "<isResultExpected>"
    Examples:
      | productName | statusCode | isResultExpected |
      | brick       | 404        | false            |
      | .?*         | 404        | false            |
      | 大           | 404        | false            |
      |             | 404        | false            |
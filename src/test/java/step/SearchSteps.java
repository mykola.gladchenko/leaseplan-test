package step;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;
import org.assertj.core.api.SoftAssertions;

import java.util.List;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;

public class SearchSteps extends ScenarioSteps {

    @Steps
    CommonSteps common;

    @When("he calls search endpoint with productName {string}")
    public void heCallsSearchEndpoint(String productName) {
        common.SearchByProductName(productName);
    }

    @Then("he sees response status code {int}")
    public void heSeesTheResultsDisplayedForApple(int statusCode) {
        restAssuredThat(response -> response.statusCode(statusCode));
    }

    @Then("he sees the results for {string}")
    public void heSeesTheResults(String productName) {
        List<String> titles = lastResponse().path("title");
        SoftAssertions.assertSoftly(softLy -> {
            for (String title : titles) {
                softLy.assertThat(title).containsIgnoringCase(productName);
            }
        });
    }

    @Then("he sees the results list displayed is {string}")
    public void heSeesTheResultsListDisplayed(String isExpected) {
        if (Boolean.parseBoolean(isExpected)) {
            restAssuredThat(response -> response.body("title", hasSize(greaterThan(0))));
        } else {
            SoftAssertions.assertSoftly(softLy -> {
                restAssuredThat(response -> response.body("detail.error", is(true)));
                restAssuredThat(response -> response.body("detail.message", is("Not found")));
            });
        }
    }
}

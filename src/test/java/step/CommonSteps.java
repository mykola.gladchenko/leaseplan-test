package step;

import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static config.ConfigProperties.BASE_URL;
import static endpoint.Endpoint.SEARCH_BY_PRODUCT_NAME;

public class CommonSteps extends ScenarioSteps {

    static {
        SerenityRest.filters(new RequestLoggingFilter(LogDetail.ALL));
        SerenityRest.filters(new ResponseLoggingFilter(LogDetail.ALL));
    }

    private static RequestSpecification given() {
        return SerenityRest
                .given()
                .baseUri(BASE_URL)
                .basePath("/api/v1")
                .contentType(ContentType.JSON);
    }

    @Step
    public void SearchByProductName(String productName) {
        given().
                get(SEARCH_BY_PRODUCT_NAME, productName);
    }

}
